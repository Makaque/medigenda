package medi;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Created by mark.geiger on 16/06/2017.
 */
public class Navigation {

    public static Navigation instance;

    private final Map<String, Navagable> routes = new HashMap<>();

    private static JFrame frame;

    private Navigation(){}

    public static Navigation getInstance(){
        if(instance == null){
            instance = new Navigation();
        }
        return instance;
    }

    public void setFrame(JFrame frame){
        Navigation.frame = frame;
    }

    boolean isRegistered(String name){
        return routes.containsKey(name);
    }

    public void registerPanel(String name, Navagable panel){
        if (!isRegistered(name)) {
            routes.put(name, panel);
        }
    }

    public void to(String name){
        if (isRegistered(name)){
            System.out.println(routes.get(name));
            frame.setContentPane(routes.get(name).getPage());
            frame.setVisible(true);
        } //TODO: Throw if wrong
    }
}
