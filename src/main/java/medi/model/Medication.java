package medi.model;


import medi.service.PersonService;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by mark on 17/06/17.
 */
@Entity
public class Medication {

    @Id
    private long id;

    private String name;

    private int amount;

    private Person user;

    public Medication(long id, long personId, String name, int amount) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.user = PersonService.getPerson(personId);
    }

    public Medication(long id, String name, int amount, Person user) {
        this.id = id;
        this.name = name;
        this.amount = amount;
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }

    public Person getUser() {
        return user;
    }


}
