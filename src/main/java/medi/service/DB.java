package medi.service;

import javax.swing.plaf.nimbus.State;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by mark.geiger on 19/06/2017.
 */
public class DB {

    private String framework = "embedded";
    private String protocol = "jdbc:derby:";

    private String[] tableNames = {
            "MEDICATION",
            "PERSON",
            "SCHEDULEITEM",
            "SCHEDULEGROUP"
    };

    private Connection conn;

    private static DB ourInstance = new DB();

    public static DB getInstance() {
        return ourInstance;
    }

    private List<String> missingTables() throws SQLException {
        ArrayList<String> missing = new ArrayList<>();
        Statement checkTables = null;
        try {
            checkTables = conn.createStatement();
            for (String tbl : tableNames) {
                try {
                    checkTables.executeQuery("SELECT count(*) as total FROM " + tbl);
                } catch (SQLException e) {
                    if (e.getSQLState().equals("42X05")) {
                        missing.add(tbl);
                    } else {
                        checkTables.close();
                        throw e;
                    }
                }
            }
            return missing;
        } finally {
            try {
                conn.commit();
                if (checkTables != null) {
                    checkTables.close();
                }
            } catch (SQLException e) {
                printSQLException(e);
            }
        }
    }

//    private boolean isDBSetup() throws SQLException {
//        Statement checkTables = null;
//        checkTables = conn.createStatement();
//        try {
//            checkTables.executeQuery("SELECT count(*) as total FROM MEDICATION");
//            checkTables.executeQuery("SELECT count(*) as total FROM PERSON");
//            checkTables.executeQuery("SELECT count(*) as total FROM SCHEDULEITEM");
//            checkTables.executeQuery("SELECT count(*) as total FROM SCHEDULEGROUP");
//            return true;
//        } catch (SQLException e) {
//            if (e.getSQLState().equals("42X05")) {
//                return false;
//            }
//            throw e;
//        } finally {
//            try {
//                conn.commit();
//                checkTables.close();
//            } catch (SQLException e) {
//                printSQLException(e);
//            }
//        }
//    }

    private void setupDB(List<String> missingTables) throws SQLException {
        Statement s = null;
        try {
            s = conn.createStatement();
            //        statements.add(s);
            for (String table : missingTables) {
                switch (table) {
                    case "MEDICATION":
                        s.execute("create table medication(id bigint, personId bigint, name varchar(120), remaining int)");
                        System.out.println("Created table medication");
                        break;
                    case "PERSON":
                        s.execute("create table person(id bigint, name varchar(120))");
                        System.out.println("Created table person");
                        break;
                    case "SCHEDULEITEM":
                        s.execute("create table ScheduleItem(id bigint, medicationId bigint, \"begin\" date, \"end\" date, instruction varchar(4000))");
                        System.out.println("Created table ScheduleItem");
                        break;
                    case "SCHEDULEGROUP":
                        s.execute("create table ScheduleGroup(id bigint, day varchar(10), \"time\" time)");
                        System.out.println("Created table ScheduleGroup");
                        break;
                }
            }
            conn.commit();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    printSQLException(e);
                }
            }
        }
    }

    private DB() {
        conn = null;
        ArrayList<Statement> statements = new ArrayList<Statement>(); // list of Statements, PreparedStatements
        PreparedStatement psInsert;
        PreparedStatement psUpdate;
        ResultSet rs = null;
        try {
            Properties props = new Properties(); // connection properties
            String dbName = "medDB"; // the name of the database

            conn = DriverManager.getConnection(protocol + dbName
                    + ";create=true", props);

            System.out.println("Connected to and created database " + dbName);
            conn.setAutoCommit(false);
            {
                ArrayList<String> missingTables = new ArrayList<>(missingTables());
                if (missingTables.size() > 0) {
                    System.out.println("DB is not set up");
                    setupDB(missingTables);
                } else {
                    System.out.println("DB is already set up");
                }
                conn.commit();
            }


        } catch (SQLException sqle) {
            printSQLException(sqle);
        } finally {
            // release all open resources to avoid unnecessary memory usage

            // ResultSet
            try {
                if (rs != null) {
                    rs.close();
                    rs = null;
                }
            } catch (SQLException sqle) {
                printSQLException(sqle);
            }

            // Statements and PreparedStatements
            int i = 0;
            while (!statements.isEmpty()) {
                // PreparedStatement extend Statement
                Statement st = (Statement) statements.remove(i);
                try {
                    if (st != null) {
                        st.close();
                        st = null;
                    }
                } catch (SQLException sqle) {
                    printSQLException(sqle);
                }
            }

            //Connection
            try {
                if (conn != null) {
                    conn.close();
                    conn = null;
                }
            } catch (SQLException sqle) {
                printSQLException(sqle);
            }
        }
    }

    /**
     * Prints details of an SQLException chain to <code>System.err</code>.
     * Details included are SQL State, Error code, Exception message.
     *
     * @param e the SQLException from which to print details.
     */
    private static void printSQLException(SQLException e) {
        // Unwraps the entire exception chain to unveil the real cause of the
        // Exception.
        while (e != null) {
            System.err.println("\n----- SQLException -----");
            System.err.println("  SQL State:  " + e.getSQLState());
            System.err.println("  Error Code: " + e.getErrorCode());
            System.err.println("  Message:    " + e.getMessage());
            e.printStackTrace();
            // for stack traces, refer to derby.log or uncomment this:
            //e.printStackTrace(System.err);
            e = e.getNextException();
        }
    }
}
