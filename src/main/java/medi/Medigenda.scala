package medi

import javax.swing._

/**
  * Created by mark on 17/06/17.
  */
object Medigenda {

  private def initApp() {
    val frame: JFrame = new JFrame("Medigenda")
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
    val nav: Navigation = Navigation.getInstance
    nav.setFrame(frame)
    nav.registerPanel(Routes.MAIN_MENU, new MainMenuPage)
    nav.registerPanel(Routes.ADD_MEDICATION, new AddMedicationPage)
    nav.registerPanel(Routes.MEDICATION, new MedicationPage)
    nav.to(Routes.MAIN_MENU)
    frame.pack()
    frame.setVisible(true)
  }

  def main(args: Array[String]) {
    initApp()
  }
}
