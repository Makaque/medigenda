package medi.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by mark on 17/06/17.
 */
@Entity
public class ScheduleItem {

    @Id
    private long id;

    private Medication medication;

    private Date begin;

    private Date end;

    private String instruction;

    public Date projectedEnd(){
        return end;
    }




}
