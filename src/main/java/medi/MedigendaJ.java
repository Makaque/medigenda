package medi;

import medi.service.DB;

import javax.swing.*;

/**
 * Created by mark.geiger on 16/06/2017.
 */
public class MedigendaJ {

    private static void initDB(){
        DB db = DB.getInstance();
    }

    private static void initApp(){
        JFrame frame = new JFrame("Medigenda");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Navigation nav = Navigation.getInstance();
        nav.setFrame(frame);
        nav.registerPanel(Routes.MAIN_MENU, new MainMenuPage());
        nav.registerPanel(Routes.ADD_MEDICATION, new AddMedicationPage());
        nav.registerPanel(Routes.MEDICATION, new MedicationPage());
        nav.to(Routes.MAIN_MENU);
        frame.pack();
        frame.setVisible(true);
    }

//    private static void createAndShowGUI(){
////        frame.setContentPane(new MainMenu());
//        frame = new JFrame("Medigenda");
//        Navigation nav = Navigation.getInstance();
//        nav.registerPanel("MainMenu", new MainMenu());
//        nav.registerPanel("Medication", new Medication());
//        Navigation.getInstance().to("MainMenu");
//        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//        frame.pack();
//        frame.setVisible(true);
//        nav.setFrame(frame);
//    }

//    public static void test(){
//        JFrame f = new JFrame("Medigenda");
//        f.setContentPane(new MainMenu().getMainPanel());
//        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//        f.pack();
//        f.setVisible(true);
//    }
//
//    public static void test2(){
//        JFrame f = new JFrame("Medigenda");
//        f.setContentPane(new Medigenda().panelMain);
//        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//        f.pack();
//        f.setVisible(true);
//    }

    public static void main(String[] args) {

        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
//        initApp();
        initDB();
//        initApp();
//        createAndShowGUI();
//        javax.swing.SwingUtilities.invokeLater(new Runnable() {
//            public void run() {
//                createAndShowGUI();
//            }
//        });
    }

}
