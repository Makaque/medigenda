package medi;

/**
 * Created by mark on 17/06/17.
 */
public class Routes {

    private Routes(){}

    public static final String MAIN_MENU = "MainMenu";
    public static final String ADD_MEDICATION = "AddMedication";
    public static final String MEDICATION = "Medication";

}
