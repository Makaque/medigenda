package medi;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.Vector;

/**
 * Created by mark.geiger on 16/06/2017.
 */
public class MedicationPage implements Navagable{
    private JPanel mainPanel;
    private JButton addMed;
    private JList medList;


    @Override
    public JPanel getPage() {
        return mainPanel;
    }

    public MedicationPage() {

        Vector<String> medications = new Vector<>();
        medications.add("med1");
        medications.add("med2");
        medications.add("med3");

        medList.setListData(medications);



    }

}
