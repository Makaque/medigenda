package medi;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by mark.geiger on 15/06/2017.
 */
public class MainMenuPage implements Navagable {
    private JPanel mainPanel;
    private JButton medPage;

    @Override
    public JPanel getPage() {
        return mainPanel;
    }

    public MainMenuPage() {

        medPage.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                Navigation.getInstance().to(Routes.ADD_MEDICATION);
//                JOptionPane.showMessageDialog(null, "Clicked");

            }
        });
    }
}
