package medi.model;

import javax.persistence.*;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by mark on 17/06/17.
 */
@Entity
public class Person {

    @Id
    private long id;

    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "medication")
    private List<Medication> medications;

    private Map<Medication, Integer> medicationDosage;

    private List<ScheduleGroup> schedule;

    public Set<Medication> getMedications(){
        return null;
    }

    public int getGroupRange(){
        return 0;
    }



}
