package medi.model;

import medi.service.DayTime;

import javax.persistence.*;

/**
 * Created by mark on 17/06/17.
 */
@Entity
public class SpecialTimes {

    @Id
    private long id;

    private String day;

    private String type;

    @AttributeOverrides({
            @AttributeOverride(name="day", column = @Column(name = "DAY")),
            @AttributeOverride(name="time", column = @Column(name = "TIME"))
    })
    private DayTime dayTime;
}
